package basic.storm.spouts;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import java.util.Map;


public class MyInputStream extends BaseRichSpout {

  private SpoutOutputCollector collector;
  private boolean completed = false;
  public void ack(Object msgId) {
    System.out.println("OK:" + msgId);
  }
  public void close() {
  }
  public void fail(Object msgId) {
    System.out.println("FAIL:" + msgId);
  }

  // Emit spout
  public void nextTuple() {
    // Do nothing once done.
    if (completed) {
      try {
        System.out.println("Printing spout message : Spout");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        // Do nothing.
      }
      return;
    }

    try {
      int counter = 5;
      while (counter > 1) {
        counter = counter - 1;
        this.collector.emit(new Values("Bolt"));
      }
    } catch (Exception e) {
      throw new RuntimeException("Error reading tuple", e);
    } finally {
      completed = true;
    }
  }

  public void open(Map conf, TopologyContext context,
                   SpoutOutputCollector collector) {
    this.collector = collector;
  }

  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields("outspout"));
  }
}

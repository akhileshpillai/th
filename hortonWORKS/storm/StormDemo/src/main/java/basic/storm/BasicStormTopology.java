package basic.storm;
import backtype.storm.Config;
import backtype.storm.LocalCluster;

import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.AuthorizationException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import basic.storm.bolts.MyProcessingUnit;
import basic.storm.spouts.MyInputStream;

/**
 * Created by akhileshpillai on 2/8/16.
 */

public class BasicStormTopology {
    public static void main(String[] args) throws InterruptedException, InvalidTopologyException, AuthorizationException, AlreadyAliveException {
        // Define topology.
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("spout-printer", new MyInputStream());
        builder.setBolt("bolt-printer", new MyProcessingUnit())
                .shuffleGrouping("spout-printer");

        // Configuration.
        Config conf = new Config();
        conf.setDebug(true);
        conf.setNumWorkers(2);

        // Run topology.
        conf.put(Config.TOPOLOGY_MAX_SPOUT_PENDING, 1);
        //LocalCluster cluster = new backtype.storm.LocalCluster();
        StormSubmitter.submitTopology("BasicStorm", conf, builder.createTopology());

        //cluster.submitTopology("BasicStorm", conf, builder.createTopology());
        //Thread.sleep(3000);

        //cluster.shutdown();
    }
}

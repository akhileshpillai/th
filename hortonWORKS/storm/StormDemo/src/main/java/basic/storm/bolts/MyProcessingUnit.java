package basic.storm.bolts;

import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import java.util.Map;

public class MyProcessingUnit extends BaseBasicBolt {

  String boutput;

  // Setup.
  @Override
  public void prepare(Map stormConf, TopologyContext context) {
    this.boutput = "somevalue";
  }

  // Print bolt output on shutdown.
  @Override
  public void cleanup() {
    System.out.println("-- cleanup Printing bolt output : " + this.boutput);

  }
  // get the value from emitted spout tuple
  @Override
  public void execute(Tuple input, BasicOutputCollector collector) {
    System.out.println("-- Printing bolt output : " + this.boutput);
    String input_from_sp = input.getStringByField("outspout");
    this.boutput = input_from_sp;
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
  }
}

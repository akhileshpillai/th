package bonus.tweet.spouts;

import java.util.Map;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterSpout extends BaseRichSpout {

  private SpoutOutputCollector collector;
  private Twitter twitterReader;
  private Query query;
  private boolean completed = false;

  public void ack(Object msgId) {
    System.out.println("OK:" + msgId);
  }

  public void close() {
  }

  public void fail(Object msgId) {
    System.out.println("FAIL:" + msgId);
  }

  // Emit each line.
  public void nextTuple() {
    // Read From Twitter.
      try {
        Utils.sleep(1000);
        System.out.println("#################--- TwitterSpout next tuple #################################");
        QueryResult result = null;
        result = this.twitterReader.search(this.query);
        for (Status status : result.getTweets()) {
          System.out.println("@" + status.getUser().getScreenName() + ":" + status.getText());
          this.collector.emit(new Values(status.getText()));
        }

      } catch (TwitterException e) {
        throw new RuntimeException("Error reading tuple", e);
      }

  }


  // Set Twitter conf and save collector.
  public void open(Map conf, TopologyContext context,
                   SpoutOutputCollector collector) {
    try {
      ConfigurationBuilder cb = new ConfigurationBuilder();
      cb.setDebugEnabled(true)
              .setOAuthConsumerKey("xxxxxxxxxxxxxxxxxxxxxxxx")
              .setOAuthConsumerSecret("xxxxxxxxxxxxxxxxxxxxxxxx")
              .setOAuthAccessToken("xxxxxxxxxxxxxxxxxxxxxxxx")
              .setOAuthAccessTokenSecret("xxxxxxxxxxxxxxxxxxxxxxxx");
      TwitterFactory tf = new TwitterFactory(cb.build());
      this.twitterReader = tf.getInstance();
      this.query = new Query(conf.get("queryString").toString());
    } catch (Exception e) {
      throw new RuntimeException(
          "Error setting conf for twitter and getting a twitter instance");
    }
    this.collector = collector;
  }

  // Declare "Tweet".
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields("tweet"));
  }
}

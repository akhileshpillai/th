package bonus.tweet.bolts;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class TweetCleanBolt extends BaseBasicBolt {

  @Override
  public void cleanup() {
  }

  // Lowercase tweet and get hashtag.
  @Override
  public void execute(Tuple input, BasicOutputCollector collector) {

    String tweet = input.getStringByField("tweet");
    System.out.println("#################--- TweetCleanBolt execute next tuple ##################### search " + tweet );
    String[] words = tweet.split(" ");
    for (String word : words) {
      word = word.trim();
      if (!word.isEmpty() && word.startsWith("#")) {
        word = word.toLowerCase();
        collector.emit(new Values(word));
      }
    }
  }

  // Emit "hashtag".
  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields("hashtag"));
  }
}

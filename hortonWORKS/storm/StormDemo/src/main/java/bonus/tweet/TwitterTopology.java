package bonus.tweet;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.AuthorizationException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;

public class TwitterTopology {
  public static void main(String[] args)
          throws InterruptedException, InvalidTopologyException, AuthorizationException, AlreadyAliveException {

    // Define topology. 
    TopologyBuilder builder = new TopologyBuilder();
    builder.setSpout("twitter-reader", new bonus.tweet.spouts.TwitterSpout());
    builder.setBolt("tweet-filter", new bonus.tweet.bolts.TweetCleanBolt())
      .shuffleGrouping("twitter-reader");
    builder.setBolt("tweet-analytic", new bonus.tweet.bolts.TweetAnalyticBolt(), 1)
      .fieldsGrouping("tweet-filter", new Fields("hashtag"));

    // Configuration.
    Config conf = new Config();
    conf.put("queryString", args[0]);
    conf.setDebug(true);
    conf.setNumWorkers(1);


    // Run topology.
    conf.put(Config.TOPOLOGY_MAX_SPOUT_PENDING, 1);
    StormSubmitter.submitTopology("BonusStorm", conf, builder.createTopology());
    //LocalCluster cluster = new LocalCluster();
    //cluster.submitTopology("Tweet Storm", conf, builder.createTopology());
    //Thread.sleep(10000);
    //cluster.shutdown();
  }
}

DROP TABLE IF EXISTS bucket_employees_parquet;

-- Create external table.
CREATE EXTERNAL TABLE bucket_employees_parquet(
  emp_no INT,
  birth_date DATE,
  first_name STRING,
  last_name STRING,
  gender STRING,
  hire_date DATE 
)
CLUSTERED BY (emp_no) INTO 10 BUCKETS
STORED AS PARQUET
LOCATION '/user/root/bucket_employees_parquet';

DROP TABLE IF EXISTS bucket_clean_salaries_parquet;

-- Create external table.
CREATE EXTERNAL TABLE bucket_clean_salaries_parquet(
  emp_no INT,
  salary INT,
  from_date STRING,
  to_date DATE 
)
CLUSTERED BY (emp_no) INTO 10 BUCKETS
STORED AS PARQUET
LOCATION '/user/root/bucket_clean_salaries_parquet';

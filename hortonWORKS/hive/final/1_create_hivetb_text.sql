DROP TABLE IF EXISTS employees_textfile;

-- Create external table.
CREATE EXTERNAL TABLE employees_textfile(
  emp_no INT,
  birth_date STRING,
  first_name STRING,
  last_name STRING,
  gender STRING,
  hire_date STRING
)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/user/root/employees_textfile';

DROP TABLE IF EXISTS salaries_textfile;

-- Create external table.
CREATE EXTERNAL TABLE salaries_textfile(
  emp_no INT,
  salary INT,
  from_date STRING,
  to_date STRING
)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/user/root/salaries_textfile';

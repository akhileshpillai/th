INSERT OVERWRITE TABLE employees_parquet
SELECT emp_no, to_date(birth_date), first_name, last_name, gender, to_date(hire_date) 
FROM employees_textfile;

INSERT OVERWRITE TABLE salaries_parquet
SELECT  emp_no, salary, from_date, date_sub(to_date,1)
FROM salaries_textfile;

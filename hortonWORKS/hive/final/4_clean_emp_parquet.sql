DROP TABLE IF EXISTS clean_employees_parquet;

-- Create external table.
CREATE EXTERNAL TABLE clean_employees_parquet(
  emp_no INT,
  birth_date DATE,
  first_name STRING,
  last_name STRING,
  gender STRING,
  hire_date DATE 
)
STORED AS PARQUET
LOCATION '/user/root/clean_employees_parquet';

INSERT OVERWRITE TABLE clean_employees_parquet
select a.emp_no,a.birth_date,a.first_name, a.last_name,a.gender, b.start_date from employees_parquet as a join  (select emp_no, min(from_date) as start_date  from salaries_parquet group by emp_no) as b on (a.emp_no = b.emp_no);

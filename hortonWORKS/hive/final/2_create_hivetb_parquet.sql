DROP TABLE IF EXISTS employees_parquet;

-- Create external table.
CREATE EXTERNAL TABLE employees_parquet(
  emp_no INT,
  birth_date DATE,
  first_name STRING,
  last_name STRING,
  gender STRING,
  hire_date DATE 
)
STORED AS PARQUET
LOCATION '/user/root/employees_parquet';

DROP TABLE IF EXISTS salaries_parquet;

-- Create external table.
CREATE EXTERNAL TABLE salaries_parquet(
  emp_no INT,
  salary INT,
  from_date STRING,
  to_date DATE 
)
STORED AS PARQUET
LOCATION '/user/root/salaries_parquet';
